# prov-table-info
 Provisioning tables info
 
 ## Usage
 Pass in the vlan id as a command line parameter
 
 `python3 main.py 104`

This specifies table 4 / vlan 104